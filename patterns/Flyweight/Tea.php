<?php

class KarakTea
{

}

class TeaMaker
{
    protected $availableTea = [];

    public function make($preference)
    {
        if (empty($this->availableTea[$preference])) {
            $this->availableTea[$preference] = new KarakTea();
        }

        return $this->availableTea[$preference];
    }
}

class TeaShop
{
    private $tea;
    private $orders = [];

    public function __construct(TeaMaker $tea)
    {
        $this->tea = $tea;
    }

    public function addOrder(int $table, int $teaType): void
    {
        $this->orders[$table] = $this->tea->make($teaType);
    }

    public function serve()
    {
        foreach ($this->orders as $table => $tea) {
            echo $tea;
        }
    }
}

$tea = new TeaMaker();
$shop = new TeaShop($tea);

$shop->addOrder(1, 2);
$shop->addOrder(2, 2);
$shop->addOrder(3, 3);
$shop->serve();