<?php


interface RenderableInterface
{
    public function render(): string;
}

class Form implements RenderableInterface
{
    private $elements;

    public function render(): string
    {
        $formCode = '<form>';

        foreach ($this->elements as $element) {
            /**
             * @var $element RenderableInterface
             */
            $formCode .= $element->render();
        }

        $formCode .= '</form>';

        return $formCode;
    }

    public function addElement(RenderableInterface $element)
    {
        $this->elements[] = $element;
    }
}

class TextInput implements RenderableInterface
{
    public function render(): string
    {
        return;
    }
}

class Button implements RenderableInterface
{
    public function render(): string
    {
        return;
    }
}

$form = new Form();
$form->addElement(new TextInput());
$form->addElement(new Button());
$form->addElement(new TextInput());
$form->addElement(new Button());
$form->render();