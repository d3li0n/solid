<?php

interface WebPage
{
    public function __construct(ThemeInterface $theme);
    public function getContent();
}

class About implements WebPage
{
    protected $theme;

    public function __construct(ThemeInterface $theme)
    {
        $this->theme = $theme;
    }

    public function getContent()
    {
        return "Страница с информацией в " . $this->theme->getColor();
    }
}

class Careers implements WebPage
{
    protected $theme;

    public function __construct(ThemeInterface $theme)
    {
        $this->theme = $theme;
    }

    public function getContent()
    {
        return "Страница карьеры в " . $this->theme->getColor();
    }
}

interface ThemeInterface
{
    public function getColor();
}

class DarkTheme implements ThemeInterface
{
    public function getColor()
    {

    }
}

class LightTheme implements ThemeInterface
{
    public function getColor()
    {

    }
}

class RoseGoldTheme implements ThemeInterface
{
    public function getColor()
    {
        // TODO: Implement getColor() method.
    }
}

$themeLight = new LightTheme();

$careers = new Careers($themeLight);
$careers->getContent();