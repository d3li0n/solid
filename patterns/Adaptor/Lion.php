<?php

interface Lion
{
    public function roar();
}

class AfricanLion implements Lion
{
    public function roar()
    {

    }
}

class AsianLion implements Lion
{
    public function roar()
    {

    }
}

class Hunter
{
    public function hunt(Lion $lion)
    {
        $lion->roar();
    }
}

class Dog
{
    public function bark()
    {

    }
}

class DogAdaptor implements Lion
{
    private $dog;

    public function __construct(Dog $dog)
    {
        $this->dog = $dog;
    }

    public function roar()
    {
        $this->dog->bark();
    }
}

$dog = new Dog();
$dogAdaptor = new DogAdaptor($dog);

$hunter = new Hunter();
$hunter->hunt($dogAdaptor);