<?php

interface Coffee
{
    public function getCost();
    public function getDescription();
}

class SimpleCoffee implements Coffee
{
    public function getCost()
    {
        return 10;
    }

    public function getDescription()
    {
        return 'Простой кофе';
    }
}

class MilkCoffee implements Coffee
{
    protected $coffee;

    public function __construct(Coffee $coffee)
    {
        $this->coffee = $coffee;
    }

    public function getCost()
    {
        return $this->coffee->getCost() + 2;
    }

    public function getDescription()
    {
        return $this->coffee->getDescription() . ', молоко';
    }
}

class VanillaCoffee implements Coffee
{
    protected $coffee;

    public function __construct(Coffee $coffee)
    {
        $this->coffee = $coffee;
    }

    public function getCost()
    {
        return $this->coffee->getCost() + 3;
    }

    public function getDescription()
    {
        return $this->coffee->getDescription() . ', ваниль';
    }
}

$simpleCoffee = new SimpleCoffee();
echo $simpleCoffee->getCost() . 'бел. рублей';

$latteCoffee = new MilkCoffee($simpleCoffee);
echo $latteCoffee->getCost();

$vanillaCoffee = new VanillaCoffee($latteCoffee);
echo $vanillaCoffee->getCost();

$vanillaSimpleCoffee = new VanillaCoffee(clone $simpleCoffee);
echo $vanillaSimpleCoffee->getCost();