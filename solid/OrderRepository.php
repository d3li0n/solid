<?php
//Second Example
class OrderRepository implements Order
{
    public function load($orderID)
    {
        $pdo = new PDO($this->config->getDsn(), $this->config->getDBUser(), $this->config->getDBPassword());
        $statement = $pdo->prepare('SELECT * FROM `orders` WHERE id=:id');
        $statement->execute(array(':id' => $orderID));
        return $query->fetchObject('Order');
    }
    public function save($order){/*...*/}
    public function update($order){/*...*/}
    public function delete($order){/*...*/}
}

//Solution
interface OrderInterface
{
    public function load($orderId);
    public function save($order);
    public function update($order);
    public function delete($order);
}

class MySql implements OrderInterface
{
    public function load($orderId)
    {

    }

    public function save($order)
    {

    }

    public function update($order)
    {

    }

    public function delete($order)
    {

    }
}

class MongoDb implements OrderInterface
{
    public function load($orderId)
    {

    }

    public function save($order)
    {

    }

    public function update($order)
    {

    }

    public function delete($order)
    {

    }
}

class PostGres implements OrderInterface
{
    public function load($orderId)
    {

    }

    public function save($order)
    {

    }

    public function update($order)
    {

    }

    public function delete($order)
    {

    }
}

class NewDb implements OrderInterface
{
    public function load($orderId)
    {

    }

    public function save($order)
    {

    }

    public function update($order)
    {

    }

    public function delete($order)
    {

    }
}