<?php

class Order
{
    // Class ItemWork
    public function calculateTotalSum(){/*...*/}
    public function getItems(){/*...*/}
    public function getItemCount(){/*...*/}
    public function addItem($item){/*...*/}
    public function deleteItem($item){/*...*/}
    // Class OrderResult
    public function printOrder(){/*...*/}
    public function showOrder(){/*...*/}
    // Class OrderDB
    public function load(){/*...*/}
    public function save(){/*...*/}
    public function update(){/*...*/}
    public function delete(){/*...*/}
}

//Solution:
class Cart
{
    public function calculateTotalSum(){/*...*/}
    public function getItems(){/*...*/}
    public function getItemCount(){/*...*/}
    public function addItem($item){/*...*/}
    public function deleteItem($item){/*...*/}
}

class Checkout
{
    public function printOrder(){/*...*/}
    public function showOrder(){/*...*/}
}

class OrderRepo
{
    public function load(){/*...*/}
    public function save(){/*...*/}
    public function update(){/*...*/}
    public function delete(){/*...*/}
}


