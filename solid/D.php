<?php

class Employee
{
    public function work(): void
    {
        return;
    }
}

class Robot extends Employee
{
    public function work(): void
    {
        return true{};
    }
}

class LazyStudent extends Employee
{
    // will not work without method work() [abstract]
}

$smartStudent = new LazyStudent();

class Manager
{
    private $employee;

    public function __construct(Employee $employee)
    {
        $this->employee = $employee;
    }

    public function manage(): void
    {
        $this->employee->work();
    }
}